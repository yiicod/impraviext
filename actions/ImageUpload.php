<?php

namespace yiicod\impraviext\actions;

/*
 * Redactor widget image upload action.
 *
 * @param string $attr Model attribute
 * @throws CHttpException
 */
use CAction;
use CHttpException;
use CUploadedFile;
use CJSON;
use Yii;

class ImageUpload extends CAction
{
    public $uploadPath;
    public $uploadUrl;
    public $uploadCreate = false;
    public $permissions = 0775;
    public $name;
    public $withParamKey = false;
    public $params = [];

    public function run($attr)
    {
        $name = empty($this->name) ? lcfirst($this->getController()->getId()) : $this->name;
        $attribute = strtolower((string) $attr);

        if ($this->uploadPath === null) {
            $path = Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'uploads';
            $this->uploadPath = realpath($path);
            if ($this->uploadPath === false && $this->uploadCreate === true) {
                if (!mkdir($path, $this->permissions, true)) {
                    throw new CHttpException(500, CJSON::encode(
                            ['error' => 'Could not create upload folder "'.$path.'".']
                    ));
                }
            }
        }
        if ($this->uploadUrl === null) {
            $this->uploadUrl = Yii::app()->request->baseUrl.'/uploads';
        }

        // Make Yii think this is a AJAX request.
        $_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest';

        $file = CUploadedFile::getInstanceByName('file');
        if ($file instanceof CUploadedFile) {
            $attributePath = $this->uploadPath.DIRECTORY_SEPARATOR.$name;
            foreach ($this->params as $param) {
                if (Yii::app()->request->getParam($param, 0)) {
                    if ($this->withParamKey) {
                        $attributePath .= DIRECTORY_SEPARATOR.$param.DIRECTORY_SEPARATOR.Yii::app()->request->getParam($param, 0);
                    } else {
                        $attributePath .= DIRECTORY_SEPARATOR.Yii::app()->request->getParam($param, 0);
                    }
                }
            }
            $attributePath .= DIRECTORY_SEPARATOR.$attribute;

            if (!in_array(strtolower($file->getExtensionName()), ['gif', 'png', 'jpg', 'jpeg'])) {
                throw new CHttpException(500, CJSON::encode(
                        ['error' => 'Invalid file extension '.$file->getExtensionName().'.']
                ));
            }
            $fileName = trim(md5($attribute.time().uniqid(rand(), true))).'.'.$file->getExtensionName();
            if (!is_dir($attributePath)) {
                if (!mkdir($attributePath, $this->permissions, true)) {
                    throw new CHttpException(500, CJSON::encode(
                            ['error' => 'Could not create folder "'.$attributePath.'". Make sure "uploads" folder is writable.']
                    ));
                }
            }
            $path = $attributePath.DIRECTORY_SEPARATOR.$fileName;
            if (file_exists($path) || !$file->saveAs($path)) {
                throw new CHttpException(500, CJSON::encode(
                        ['error' => 'Could not save file or file exists: "'.$path.'".']
                ));
            }

            $attributeUrl = $this->uploadUrl.DIRECTORY_SEPARATOR.$name;
            foreach ($this->params as $param) {
                if (Yii::app()->request->getParam($param, 0)) {
                    if ($this->withParamKey) {
                        $attributeUrl .= DIRECTORY_SEPARATOR.$param.DIRECTORY_SEPARATOR.Yii::app()->request->getParam($param, 0);
                    } else {
                        $attributeUrl .= DIRECTORY_SEPARATOR.Yii::app()->request->getParam($param, 0);
                    }
                }
            }
            $attributeUrl .= DIRECTORY_SEPARATOR.$attribute.DIRECTORY_SEPARATOR.$fileName;
            $data = [
                'filelink' => $attributeUrl,
            ];
            echo CJSON::encode($data);
            exit;
        } else {
            throw new CHttpException(500, CJSON::encode(
                    ['error' => 'Could not upload file.']
            ));
        }
    }
}
