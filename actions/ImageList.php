<?php

namespace yiicod\impraviext\actions;

/*
 * Redactor widget image list action.
 *
 * @param string $attr Model attribute
 */
use CAction;
use CFileHelper;
use CJSON;
use Yii;

class ImageList extends CAction
{
    public $uploadPath;
    public $uploadUrl;
    public $name;

    public function run($attr)
    {
        $name = empty($this->name) ? lcfirst($this->getController()->getId()) : $this->name;
        $attribute = strtolower((string) $attr);

        if ($this->uploadPath === null) {
            $path = Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'uploads';
            $this->uploadPath = realpath($path);
            if ($this->uploadPath === false) {
                exit;
            }
        }
        if ($this->uploadUrl === null) {
            $this->uploadUrl = Yii::app()->request->baseUrl.'/uploads';
        }

        $attributePath = $this->uploadPath.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR.$attribute;
        $attributeUrl = $this->uploadUrl.'/'.$name.'/'.$attribute.'/';

        $files = CFileHelper::findFiles($attributePath, ['fileTypes' => ['gif', 'png', 'jpg', 'jpeg']]);
        $data = [];
        if ($files) {
            foreach ($files as $file) {
                $data[] = [
                    'thumb' => $attributeUrl.basename($file),
                    'image' => $attributeUrl.basename($file),
                ];
            }
        }
        echo CJSON::encode($data);
        exit;
    }
}
